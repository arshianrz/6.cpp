#ifndef FIBBO_H
#define FIBBO_H


class Fibbo
{
private:
    int letter_;
public:
    Fibbo();
    void setLetter(int);
    int getLetter();
    int fibbonacci(int);
    void display();
    void run();
};

#endif // FIBBO_H
