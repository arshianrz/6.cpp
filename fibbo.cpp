#include "fibbo.h"
#include <iostream>

using namespace std;

Fibbo::Fibbo()
{}
//****************************************************************************************************************
void Fibbo::setLetter(int N)
{
    letter_=N;
}
//****************************************************************************************************************
int Fibbo::getLetter()
{
    return letter_;
}
//****************************************************************************************************************
int Fibbo::fibbonacci(int p)
{
    if (p==1 || p==2)
    {
        return 1;
    }
    else
        return fibbonacci(p-1)+fibbonacci(p-2);
}
//****************************************************************************************************************
void Fibbo::display()
{
    for(int i=1 ; i<=letter_ ; i++)
    cout<<fibbonacci(i)<<"  ";

    cout<<endl;
}
//****************************************************************************************************************
void Fibbo::run()
{
    int x;
    cout<<"Enter a random fibonacci sequence letter. \n";
    cin>>x;

    setLetter(x);
    getLetter();
    fibbonacci(x);
    display();
}
